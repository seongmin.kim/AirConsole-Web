# Samsung DMS Web Controller

Samsung DMS Web Controller for Node.js

## API

### 1. Listing DMS hosts (Controllers) :
### /api/controllers
Method: GET

- Returns list of controllers in domain

The response will be in JSON array format. Each JSON object has the key of 'location', and an Array's index is an identifier of the controller.

#### Using curl
```sh
curl http://{domain}/api/controllers \
-H "Accept :application/json"
```

#### Example JSON response
```javascript
[
    {
        "location": "북악관"
    },
    {
        "location": "은주관"
    }
]
```

### 2. Get specific DMS host (Controllers) :
### /api/controllers/{id}
Method: GET

- Returns controller object which is associated with given id.

The response will be in JSON format, and single JSON object. It has the key of 'location' whose value is an identifier of the controller.

#### Using curl
```sh
curl http://{domain}/api/controllers/{id} \
-H "Accept :application/json"
```

#### Example JSON response (id = 0)
```javascript
{
	"location": "북악관"
}
```

### 3. Listing indoors :
### /api/controllers/{id}/indoors
Method: GET

- Returns indoors which is associated with given controller id.

The response will be in JSON array format, which are ordered by the name of indoors. Each JSON object has following keys.

- address : (String) An identifier of the indoor. (xx.xx.xx)
- roomTemp : (Integer) Current room temperature in Celsius.
- setTemp : (Integer) Demanding temperature in Celsius. (18°C ~ 30°C)
- airSwingLR : (String) Horizontal swing of wings. ("true" / "false")
- airSwingUD : (String) Vertical swing of wings. ("true" / "false")
- fanSpeed : (String) Spinning speed of the fan. ("auto" / "low" / "mid" / "high")
- operationMode : (String) Operation mode of the indoor. ("auto" / "cool" / "dry" / "fan" / "heat")
- power : (String) Power state of the indoor. ("on" / "off")
- remoconEnable : (String) Representing if a user is able to use a remote controller or not. ("true" / "false")
- name : (String) Location of the indoor.

#### Using curl
```sh
curl http://{domain}/api/controllers/{id}/indoors \
-H "Accept :application/json"
```

#### Example JSON response (id = 1)
```javascript
[
    {
        "address":"03.00.04",
        "roomTemp":25,
        "setTemp":21,
        "airSwingLR":"false",
        "airSwingUD":"false",
        "fanSpeed":"high",
        "operationMode":"cool",
        "power":"off",
        "remoconEnable":"true",
        "name":"1F창고1"
    },
    {
        "address":"03.00.05",
        "roomTemp":26,
        "setTemp":22,
        "airSwingLR":"false",
        "airSwingUD":"false",
        "fanSpeed":"auto",
        "operationMode":"auto",
        "power":"off",
        "remoconEnable":"true",
        "name":"1F창고2"
    },
    {
        "address":"03.01.06",
        "roomTemp":27,
        "setTemp":24,
        "airSwingLR":"false",
        "airSwingUD":"false",
        "fanSpeed":"auto",
        "operationMode":"cool",
        "power":"off",
        "remoconEnable":"true",
        "name":"201호"
    },
    {
        "address":"03.01.07",
        "roomTemp":26,
        "setTemp":24,
        "airSwingLR":"false",
        "airSwingUD":"false",
        "fanSpeed":"auto",
        "operationMode":"cool",
        "power":"off",
        "remoconEnable":"true",
        "name":"202호"
    },
    {
        "address":"03.01.08",
        "roomTemp":26,
        "setTemp":24,
        "airSwingLR":"false",
        "airSwingUD":"false",
        "fanSpeed":"auto",
        "operationMode":"cool",
        "power":"off",
        "remoconEnable":"true",
        "name":"202호"
    },
    {
        "address":"03.01.09",
        "roomTemp":26,
        "setTemp":19,
        "airSwingLR":"false",
        "airSwingUD":"false",
        "fanSpeed":"high",
        "operationMode":"cool",
        "power":"off",
        "remoconEnable":"true",
        "name":"203호"
    },
    {
        "address":"03.01.10",
        "roomTemp":26,
        "setTemp":19,
        "airSwingLR":"false",
        "airSwingUD":"false",
        "fanSpeed":"high",
        "operationMode":"cool",
        "power":"off",
        "remoconEnable":"true",
        "name":"203호"
    },
    {
        "address":"03.01.11",
        "roomTemp":26,
        "setTemp":25,
        "airSwingLR":"false",
        "airSwingUD":"true",
        "fanSpeed":"low",
        "operationMode":"cool",
        "power":"off",
        "remoconEnable":"true",
        "name":"204호"
    },
    {
        "address":"03.01.12",
        "roomTemp":28,
        "setTemp":18,
        "airSwingLR":"false",
        "airSwingUD":"false",
        "fanSpeed":"high",
        "operationMode":"cool",
        "power":"off",
        "remoconEnable":"true",
        "name":"205호"
    },
    {
        "address":"03.01.13",
        "roomTemp":27,
        "setTemp":24,
        "airSwingLR":"false",
        "airSwingUD":"false",
        "fanSpeed":"auto",
        "operationMode":"cool",
        "power":"off",
        "remoconEnable":"true",
        "name":"206호"
    },
    {
        "address":"03.01.14",
        "roomTemp":27,
        "setTemp":24,
        "airSwingLR":"false",
        "airSwingUD":"false",
        "fanSpeed":"auto",
        "operationMode":"cool",
        "power":"off",
        "remoconEnable":"true",
        "name":"206호"
    },
    {
        "address":"04.02.00",
        "roomTemp":24,
        "setTemp":23,
        "airSwingLR":"false",
        "airSwingUD":"false",
        "fanSpeed":"high",
        "operationMode":"cool",
        "power":"off",
        "remoconEnable":"true",
        "name":"301호"
    },
    {
        "address":"04.02.01",
        "roomTemp":24,
        "setTemp":24,
        "airSwingLR":"false",
        "airSwingUD":"false",
        "fanSpeed":"auto",
        "operationMode":"cool",
        "power":"off",
        "remoconEnable":"true",
        "name":"302-1호"
    },
    {
        "address":"04.02.02",
        "roomTemp":25,
        "setTemp":24,
        "airSwingLR":"false",
        "airSwingUD":"false",
        "fanSpeed":"auto",
        "operationMode":"cool",
        "power":"off",
        "remoconEnable":"true",
        "name":"302호"
    },
    {
        "address":"04.02.03",
        "roomTemp":22,
        "setTemp":22,
        "airSwingLR":"false",
        "airSwingUD":"false",
        "fanSpeed":"auto",
        "operationMode":"auto",
        "power":"on",
        "remoconEnable":"true",
        "name":"303-1호"
    },
    {
        "address":"04.02.04",
        "roomTemp":23,
        "setTemp":24,
        "airSwingLR":"false",
        "airSwingUD":"false",
        "fanSpeed":"auto",
        "operationMode":"cool",
        "power":"off",
        "remoconEnable":"true",
        "name":"303호"
    },
    {
        "address":"02.08.00",
        "roomTemp":22,
        "setTemp":20,
        "airSwingLR":"false",
        "airSwingUD":"false",
        "fanSpeed":"auto",
        "operationMode":"cool",
        "power":"off",
        "remoconEnable":"true",
        "name":"304-1호"
    },
    {
        "address":"02.08.01",
        "roomTemp":22,
        "setTemp":22,
        "airSwingLR":"false",
        "airSwingUD":"false",
        "fanSpeed":"auto",
        "operationMode":"auto",
        "power":"on",
        "remoconEnable":"true",
        "name":"304호"
    },
    {
        "address":"02.08.03",
        "roomTemp":23,
        "setTemp":18,
        "airSwingLR":"true",
        "airSwingUD":"true",
        "fanSpeed":"auto",
        "operationMode":"cool",
        "power":"off",
        "remoconEnable":"false",
        "name":"305-1호"
    },
    {
        "address":"03.00.02",
        "roomTemp":26,
        "setTemp":21,
        "airSwingLR":"false",
        "airSwingUD":"true",
        "fanSpeed":"high",
        "operationMode":"cool",
        "power":"off",
        "remoconEnable":"true",
        "name":"학교기업 실습1"
    },
    {
        "address":"03.00.03",
        "roomTemp":26,
        "setTemp":21,
        "airSwingLR":"false",
        "airSwingUD":"false",
        "fanSpeed":"high",
        "operationMode":"cool",
        "power":"off",
        "remoconEnable":"true",
        "name":"학교기업 실습2"
    },
    {
        "address":"03.00.00",
        "roomTemp":27,
        "setTemp":23,
        "airSwingLR":"false",
        "airSwingUD":"false",
        "fanSpeed":"auto",
        "operationMode":"cool",
        "power":"off",
        "remoconEnable":"true",
        "name":"학교기업1"
    },
    {
        "address":"03.00.01",
        "roomTemp":27,
        "setTemp":21,
        "airSwingLR":"false",
        "airSwingUD":"false",
        "fanSpeed":"auto",
        "operationMode":"cool",
        "power":"off",
        "remoconEnable":"true",
        "name":"학교기업2"
    }
]
```

### 4. Get specific indoors :
### /api/controllers/{id}/indoors/{indoor_address}
Method: GET

- Returns indoors which is associated with given controller id and indoor addresses.
- Addresses are comma seperated values.

The response will be in JSON array format, which are ordered by the name of indoors. Even if it queried a single indoor address, still the response will be a JSON array with length of 1.

#### Using curl
```sh
curl http://{domain}/api/controllers/{id}/indoors/{indoor_addresses} \
-H "Accept :application/json"
```

#### Example JSON response (id = 0, indoor_addresses = 05.00.03,05.00.04)
```javascript
[
    {
        "address":"05.00.03",
        "roomTemp":27,
        "setTemp":23,
        "airSwingLR":"false",
        "airSwingUD":"false",
        "fanSpeed":"auto",
        "operationMode":"cool",
        "power":"off",
        "remoconEnable":"true",
        "name":"101 강의실1"
    },
    {
        "address":"05.00.04",
        "roomTemp":26,
        "setTemp":22,
        "airSwingLR":"false",
        "airSwingUD":"false",
        "fanSpeed":"auto",
        "operationMode":"cool",
        "power":"off",
        "remoconEnable":"true",
        "name":"102 강의실2"
    }
]
```

### 5. Update states of indoors :
### /api/controllers/{id}/indoors/{indoor_address}
Method: POST

- Updates indoors' state which is associated with given controller id and indoor addresses.
- Addresses are comma seperated values which means that it is possible to update multiple indoors at once.

The following should be sent as a form parameter (Content-Type: application/x-www-form-urlencoded).

- power : Power state of the indoor. ("on" / "off")
- temperature : Demanding temperature in Celsius. (18 ~ 30)
- fanSpeed : Spinning speed of the fan. ("auto" / "low" / "mid" / "high")
- airSwingUD : Vertical swing of wings. ("true" / "false")
- airSwingLR : Horizontal swing of wings. ("true" / "false")
- operationMode : Operation mode of the indoor. ("auto" / "cool" / "dry" / "fan" / "heat")

#### Using curl
```sh
curl http://{domain}/api/controllers/{id}/indoors/{indoor_addresses} \
-H "Content-Type :application/x-www-form-urlencoded" \
-d 'power=on&temperature=22&fanSpeed=auto&airSwingUD=false&airSwingLR=false&operationMode=auto' \
-X POST
```

#### Response
- Status 200: Indoors' status has been updated successfully.
- Status 400: If the parameter is empty or has wrong value.
