const express = require('express')
const api = require('./api/api.js')
const app = express()

app.use(express.static(__dirname + '/public'))
app.use((req, res, next) => {	//	Access Logger
	console.log(`${req.ip} - - [${new Date().toLocaleString()}] "${req.method} ${req.originalUrl} ${req.protocol}/${req.httpVersion}" -`)
	next()
})
app.use('/api', api)

app.set('views', './views')
app.set('view engine', 'pug')

app.get('/hidden', (req, res) => {
	res.render('index')
})

app.listen(80, () => { console.log('Server is running on port 8080!') })
