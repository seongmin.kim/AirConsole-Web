const request = require('sync-request')
const cheerio = require('cheerio')
const Cookie = require('request-cookies').Cookie
const RSAKey = require('./RSAKey')
const queryString = require('query-string')

exports.getSessionId = function (host, dmsHostUser, dmsHostPass, login_path) {
	const resGetSessionId = request('GET', 'http://' + host)

	const rawCookies = resGetSessionId.headers['set-cookie']
	if (!rawCookies) return null
	const cookie = new Cookie(rawCookies[0])
	const sessionID = cookie.value

	const $ = cheerio.load(resGetSessionId.getBody('utf-8'))
	const rsaPublicKeyModulus = $('#rsaPublicKeyModulus').val()
	const rsaPublicKeyExponent = $('#rsaPublicKeyExponent').val()

	if (!rsaPublicKeyModulus || !rsaPublicKeyExponent) return null
		
	const rsa = new RSAKey()
	rsa.setPublic(rsaPublicKeyModulus, rsaPublicKeyExponent)

	const securedUsername = rsa.encrypt(dmsHostUser + sessionID)
	const securedPassword = rsa.encrypt(dmsHostPass + sessionID)

	const securedUserInfo = queryString.stringify({
		'securedUsername': securedUsername,
		'securedPassword': securedPassword
	})


	const resValidateSessionId = request('POST', 'http://' + host + login_path, {
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Cookie': cookie.key + '=' + sessionID,
			'Content-Length': Buffer.byteLength(securedUserInfo)
		},
		body: securedUserInfo
	})

	return sessionID
}
