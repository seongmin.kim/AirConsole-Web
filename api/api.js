const fs = require('fs')
const express = require('express')
const bodyParser = require('body-parser')
const router = express.Router()

const session = require('./get-session')
const AirConsole = require('./AirConsole')

const dms_hosts = JSON.parse(fs.readFileSync(__dirname + '/dms_info/dms_hosts.json'))
const dms_data_check = JSON.parse(fs.readFileSync(__dirname + '/dms_info/dms_valid_data.json'))

let controllers = []
dms_hosts.forEach((dms_host) => {
	controllers.push({
		host: new AirConsole({
			name: dms_host.location,
			host: dms_host.host,
			port: dms_host.port,
			firmware: dms_host.firmware,
			sessionId: session.getSessionId(dms_host.host, dms_host.user, dms_host.pass, dms_host.login_path)
		}),
		indoor_list: []
	})
	const {host} = controllers[controllers.length - 1]
	console.log(`${host._spec.name} ${host._spec.firmware} ${host._spec.sessionId} Loaded Successfully`)
})

function updateIndoors(controllers) {
	controllers.forEach(async (controller, index) => {
		await controller.host.readAll().then(data => {
			console.log(controller.host._spec.name + ` : data arrrived at ${new Date().toLocaleString()}`)
			controller.indoor_list = data
		}).catch(e => {throw e})
	})
}

//	Fetch indoor data from server every 30 seconds
console.log('Retrieving Indoor List ...')
updateIndoors(controllers)
setInterval(updateIndoors, 180000, controllers)

//	Enable x-www-form-urlencoded Parsing
router.use(bodyParser.urlencoded({ extended: true }))

/**
 * retrieve all controllers (hosts)
 */
router.get('/controllers', (req, res) => {
	let controllers_client = []
	controllers.forEach((controller) => {
		controllers_client.push({
			location: controller.host._spec.name
		})
	})

	res.json(controllers_client)
})

/**
 * check if the host exists.
 * if not, ends the request-response cycle.
 */
function check_host(req, res, next) {
	const {host_id} = req.params
	if (host_id < 0 || host_id >= dms_hosts.length) res.status(400).send('Invalid Host ID')
	else next()
}

/**
 * retrieve details of the controller
 */
router.get('/controllers/:host_id', check_host, (req, res) => {
	const {host_id} = req.params

	res.json({
		location: controllers[host_id]._spec.name
	})
})

/**
 * retrieve all indoors
 */
router.get('/controllers/:host_id/indoors', check_host, (req, res) => {
	const {host_id} = req.params

	res.json(controllers[host_id].indoor_list)
})

/**
 * GET : retrieve details of the indoor
 * POST : Update the state of the indoor
 */
router.route('/controllers/:host_id/indoors/:indoor_addresses')
	.get(check_host, (req, res) => {
		const {host_id, indoor_addresses} = req.params

		const indoorsToSearch = indoor_addresses.split(',')

		res.json(controllers[host_id].indoor_list.filter(indoor => indoorsToSearch.includes(indoor.address)))
	})
	.post(check_host, (req, res) => {
		const {host_id, indoor_addresses} = req.params

		const indoorsToUpdate = indoor_addresses.split(',')

		const { power, fanSpeed, airSwingUD, airSwingLR, operationMode } = req.body
		const temperature = parseInt(req.body.temperature)

		let affectedIndoors = 0

		if (dms_data_check.power.includes(power) && dms_data_check.temperature.includes(temperature) && 
			dms_data_check.fanSpeed.includes(fanSpeed) && dms_data_check.airSwingLR.includes(airSwingLR) &&
			dms_data_check.airSwingUD.includes(airSwingUD) && dms_data_check.operationMode.includes(operationMode)) {
			//	TODO: Send a request
			controllers[host_id].host.update(
				indoorsToUpdate, {
				power: power,
				temperature: temperature,
				fanSpeed: fanSpeed,
				airSwingUD: airSwingUD,
				airSwingLR: airSwingLR,
				operationMode: operationMode
			})

			//	Update Internal indoor_list
			controllers[host_id].indoor_list.forEach(
				(indoor) => {
					if (indoorsToUpdate.includes(indoor.address)) {
						indoor.power = power
						indoor.setTemp = temperature
						indoor.fanSpeed = fanSpeed
						indoor.airSwingUD = airSwingUD
						indoor.airSwingLR = airSwingLR
						indoor.operationMode = operationMode

						affectedIndoors++
					}
				}
			)
			res.status(200).send(`Server says : "${affectedIndoors}개의 실내기가 영향을 받음" (${new Date().toLocaleString()})`)
		}
		else {
			res.status(400).send('Invalid Request')
		}

		// console.log(controllers[host_id].indoor_list)
	})

module.exports = router
