let bigInt = require('big-integer')
let secureRandom = require('secure-random')

module.exports = class RSAKey {

	constructor() {
		this.n = null
		this.e = 0
	}

	doPublic(x) {
		return x.modPow(this.e, this.n)
	}

	setPublic(N, E) {
		if (N != null && E != null && N.length > 0 && E.length > 0) {
			this.n = this.parseBigInt(N, 16)
			this.e = parseInt(E, 16)
		}
		else {
			console.log('Invalid RSA Public Key')
		}
	}

	encrypt(text) {
		let m = this.pkcs1pad2(text, (this.n.toString(2).length + 7) >> 3)
		if (m == null) return null
		let c = this.doPublic(m)
		if (c == null) return null
		let h = c.toString(16)
		if ((h.length & 1) == 0) return h; else return '0' + h
	}

	pkcs1pad2(s, n) {
		if (n < s.length + 11) { // TODO: fix for utf-8
			console.log('Message too long for RSA')
			return null
		}
		let ba = []
		let i = s.length - 1
		while (i >= 0 && n > 0) {
			let c = s.charCodeAt(i--)

			if (c < 128) {
				ba[--n] = c
			}
			else if ((c > 127) && (c < 2048)) {
				ba[--n] = (c & 63) | 128
				ba[--n] = (c >> 6) | 192
			}
			else {
				ba[--n] = (c & 63) | 128
				ba[--n] = ((c >> 6) & 63) | 128
				ba[--n] = (c >> 12) | 224
			}
		}
		ba[--n] = 0
		let x = []
		while (n > 2) { // random non zero pad
			x[0] = 0
			while (x[0] == 0) {
				x = secureRandom(1)
			}
			ba[--n] = x[0]
		}
		ba[--n] = 2
		ba[--n] = 0
		return bigInt.fromArray(ba, 256)
	}

	parseBigInt(str, r) {
		return bigInt(str, r)
	}

}

