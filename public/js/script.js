let currentController = 0
let selectedIndoors = new Set()

const operationMode_dic_e2k = {
	auto: '자동',
	cool: '냉방',
	dry: '제습',
	fan: '송풍',
	heat: '난방'
}
const operationMode_dic_k2e = {
	'자동': 'auto',
	'냉방': 'cool',
	'제습': 'dry',
	'송풍': 'fan',
	'난방': 'heat'
}
const fanSpeed_dic_e2k = {
	auto: '자동',
	low: '미풍',
	mid: '약풍',
	high: '강풍'
}
const fanSpeed_dic_k2e = {
	'자동': 'auto',
	'미풍': 'low',
	'약풍': 'mid',
	'강풍': 'high'
}

$(function () {
	//	Get Controllers (Navigation Bar)
	$.getJSON('/api/controllers', function (controllers) {
		//	Add Controllers to navigation bar
		controllers.forEach(function (controller) {
			$('<li><a>' + controller.location + '</a></li>').appendTo('#controllers > ul')
		})
		//	and event handler
		$('#controllers > ul > li > a').click(function () {
			//	Set currentController to current controller's index
			currentController = $('#controllers > ul > li > a').index(this)
			$(this).parent().siblings().removeClass('is-active')
			$(this).parent().addClass('is-active')

			//	Initialize table, Empty selectedIndoors and Server Message
			$('tr:not(.has-text-centered)').remove()
			selectedIndoors.clear()
			//	Emit selectedIndoorsChanged event
			$('#groupController').trigger('selectedIndoorsChanged')
			$('#stdout').text('')

			//	Insert Rows (Indoors)
			getIndoors(currentController)
		})

		//	At First Load, Get Indoors of the first controller
		$('#controllers > ul > li:first').addClass('is-active')
		getIndoors(currentController)
	})

	//	Select All Button
	$('#btnSelectAll').click(function () {
		$('tr:not(.has-text-centered)').addClass('is-selected')
		$('tr:not(.has-text-centered)').find('.button').addClass('is-inverted')

		// TODO: Add all indoors to selected indoor list
		$('tr:not(.has-text-centered)')
			.find('span.address')
			.each(function () { selectedIndoors.add($(this).text()) })

		//	Emit selectedIndoorsChanged event
		$('#groupController').trigger('selectedIndoorsChanged')
	})

	//	Deselect All Button
	$('#btnDeselectAll').click(function () {
		$('tr:not(.has-text-centered)').removeClass('is-selected')
		$('tr:not(.has-text-centered)').find('.button').removeClass('is-inverted')

		// TODO: Empty selected indoor list
		selectedIndoors.clear()

		//	Emit selectedIndoorsChanged event
		$('#groupController').trigger('selectedIndoorsChanged')
	})

	//	GroupController Popup Event Handler
	$('#groupController').on('selectedIndoorsChanged', function () {
		$(this).find('span#selectedIndoorsCnt').text(selectedIndoors.size)
		if (selectedIndoors.size > 0) {
			$(this).addClass('is-active')
		}
		else {
			$(this).removeClass('is-active')
		}
	})

	$('#groupPower').click(function () {
		event.stopPropagation()
		$(this).toggleClass('is-info is-danger')
	})

	$('#groupCool, #groupDry, #groupHeat').click(function () {
		event.stopPropagation()
		$(this).parent().siblings().removeClass('is-active')
		$(this).parent().addClass('is-active')
	})

	$('#groupApply').click(function () {
		const power = $('#groupPower').hasClass('is-info') ? 'on' : 'off'
		const temperature = parseInt($('#groupTemperature').val())
		const fanSpeed = 'auto'
		const airSwingUD = 'false'
		const airSwingLR = 'false'
		const operationMode = operationMode_dic_k2e[$('#groupOperationMode').find('.is-active > a').text()]

		$.post(`/api/controllers/${currentController}/indoors/${Array.from(selectedIndoors).join(',')}`, {
			power: power,
			temperature: temperature,
			fanSpeed: 'auto',
			airSwingUD: 'false',
			airSwingLR: 'false',
			operationMode: operationMode
		})
			.done(function (data) {
				$('#stdout').text(data)

				// Set power
				$('tr.is-selected').find('.power').removeClass('is-info is-danger')
				if (power === 'on') {
					$('tr.is-selected').find('.power').addClass('is-info')
				}
				else {
					$('tr.is-selected').find('.power').addClass('is-danger')
				}
				
				//	Set temperature
				$('tr.is-selected').find('.setTemp').text(temperature)
				//	Set Fanspeed
				$('tr.is-selected').find('.fanSpeed').text('자동')
				//	Set airSwingUD
				$('tr.is-selected').find('.airSwingUD').removeClass('is-warning is-active')
				//	Set airSwingLR
				$('tr.is-selected').find('.airSwingLR').removeClass('is-warning is-active')
				//	Set Operation Mode
				$('tr.is-selected').find('.operationMode').text(operationMode_dic_e2k[operationMode])

				$('#btnDeselectAll').click()
			})
	})
})

/**
 * Get indoor status of the row
 * @param {JQuery} row 
 * returns { power, temperature, fanSpeed, airSwingUD, airSwingLR, operationMode }
 */
function getStatusOfRow(row) {
	const power = row.find('.power').hasClass('is-info') ? 'on' : 'off'
	const temperature = row.find('.setTemp').text()
	const fanSpeed = fanSpeed_dic_k2e[row.find('.fanSpeed').text()]
	const airSwingUD = row.find('.airSwingUD').hasClass('is-active') ? 'true' : 'false'
	const airSwingLR = row.find('.airSwingLR').hasClass('is-active') ? 'true' : 'false'
	const operationMode = operationMode_dic_k2e[row.find('.operationMode').text()]

	return {
		power: power,
		temperature: temperature,
		fanSpeed: fanSpeed,
		airSwingUD: airSwingUD,
		airSwingLR: airSwingLR,
		operationMode: operationMode
	}
}

/**
 * Get Indoors and fill the table
 * @param {number} controller 		index of the controller
 */
function getIndoors(controller) {
	//	Get Indoors
	$.getJSON(`/api/controllers/${controller}/indoors`, function (indoors) {
		const btnPower = function (power) {
			return `
				<a class="power button ${power === 'on' ? 'is-info' : 'is-danger'}">
					<i class="fa fa-power-off fa-lg"></i>
				</a>
			`
		}
		const ddSetTemp = function (setTemp) {
			return `
				<div class="dropdown">
					<div class="dropdown-trigger">
						<a class="button">
							<span class="setTemp">${setTemp}</span>
							<span class="icon is-small">
								<i class="fa fa-angle-down"></i>
							</span>
						</a>
					</div>
					<div class="dropdown-menu">
						<div class="dropdown-content">
							<a class="dropdown-item ${setTemp == 18 ? 'is-active' : ''}">18</a>
							<a class="dropdown-item ${setTemp == 19 ? 'is-active' : ''}">19</a>
							<a class="dropdown-item ${setTemp == 20 ? 'is-active' : ''}">20</a>
							<a class="dropdown-item ${setTemp == 21 ? 'is-active' : ''}">21</a>
							<a class="dropdown-item ${setTemp == 22 ? 'is-active' : ''}">22</a>
							<a class="dropdown-item ${setTemp == 23 ? 'is-active' : ''}">23</a>
							<a class="dropdown-item ${setTemp == 24 ? 'is-active' : ''}">24</a>
							<a class="dropdown-item ${setTemp == 25 ? 'is-active' : ''}">25</a>
							<a class="dropdown-item ${setTemp == 26 ? 'is-active' : ''}">26</a>
							<a class="dropdown-item ${setTemp == 27 ? 'is-active' : ''}">27</a>
							<a class="dropdown-item ${setTemp == 28 ? 'is-active' : ''}">28</a>
							<a class="dropdown-item ${setTemp == 29 ? 'is-active' : ''}">29</a>
							<a class="dropdown-item ${setTemp == 30 ? 'is-active' : ''}">30</a>
						</div>
					</div>
				</div>
			`
		}
		const ddOpMode = function (opMode) {
			
			return `
				<div class="dropdown">
					<div class="dropdown-trigger">
						<a class="button">
							<span class="operationMode">${operationMode_dic_e2k[opMode]}</span>
							<span class="icon is-small">
								<i class="fa fa-angle-down"></i>
							</span>
						</a>
					</div>
					<div class="dropdown-menu">
						<div class="dropdown-content">
							<a class="dropdown-item ${opMode === 'auto' ? 'is-active' : ''}">자동</a>
							<a class="dropdown-item ${opMode === 'cool' ? 'is-active' : ''}">냉방</a>
							<a class="dropdown-item ${opMode === 'dry' ? 'is-active' : ''}">제습</a>
							<a class="dropdown-item ${opMode === 'fan' ? 'is-active' : ''}">송풍</a>
							<a class="dropdown-item ${opMode === 'heat' ? 'is-active' : ''}">난방</a>
						</div>
					</div>
				</div>
			`
		}
		const ddFanSpeed = function (fanSpeed) {
			return `
				<div class="dropdown">
					<div class="dropdown-trigger">
						<a class="button">
							<span class="fanSpeed">${fanSpeed_dic_e2k[fanSpeed]}</span>
							<span class="icon is-small">
								<i class="fa fa-angle-down"></i>
							</span>
						</a>
					</div>
					<div class="dropdown-menu">
						<div class="dropdown-content">
							<a class="dropdown-item ${fanSpeed === 'auto' ? 'is-active' : ''}">자동</a>
							<a class="dropdown-item ${fanSpeed === 'low' ? 'is-active' : ''}">미풍</a>
							<a class="dropdown-item ${fanSpeed === 'mid' ? 'is-active' : ''}">약풍</a>
							<a class="dropdown-item ${fanSpeed === 'high' ? 'is-active' : ''}">강풍</a>
						</div>
					</div>
				</div>
			`
		}
		const btnAirSwing = function (airSwingUD, airSwingLR) {
			return `
				<div class="columns is-gapless">
					<div class="column">
						<a class="airSwingUD button ${airSwingUD === 'true' ? 'is-warning is-active' : ''}">상하</a>
					</div>
					<div class="column">
						<a class="airSwingLR button ${airSwingLR === 'true' ? 'is-warning is-active' : ''}">좌우</a>
					</div>
				</div>
			`
		}

		let index = 0
		indoors.forEach(function (indoor) {
			const { address, name, power, roomTemp, setTemp,
				operationMode, fanSpeed, airSwingLR,
				airSwingUD, remoconEnable } = indoor
			const row = `
				<tr>
					<th><span class="address" hidden>${address}</span>${++index}</th>
					<td>${name}</td>
					<td>${btnPower(power)}</td>
					<td class="roomTemp has-text-centered is-hidden-mobile">${roomTemp}</td>
					<td>${ddSetTemp(setTemp)}</td>
					<td class="is-hidden-mobile">${ddOpMode(operationMode)}</td>
					<td class="is-hidden-mobile">${ddFanSpeed(fanSpeed)}</td>
					<td class="is-hidden-mobile">${btnAirSwing(airSwingUD, airSwingLR)}</td>
				</tr>
			`
			$(row).appendTo('tbody')
		})
	})
	//	After appending all elements, Add an Event Listener to each elements
	.done(function () {
		//	Dropdown Menus
		$('.dropdown').click(function () {
			//	On Clicking Dropdown Menu, Spread the Dropdown
			event.stopPropagation()
			$(this).toggleClass('is-active')
		})

		$(document).click(function () {
			//	On Clicking Background, Collapse All Dropdowns
			$('.dropdown').removeClass('is-active')
		})

		//	Dropown Items
		$('.dropdown-item').click(function () {
			$(this).siblings().removeClass('is-active')
			$(this).addClass('is-active')

			$(this).parents('.dropdown').find('span').eq(0).text($(this).text())

			// TODO: Send a Request
			const myRow = $(this).parents('tr')
			$.post(`/api/controllers/${controller}/indoors/${myRow.find('span.address').text()}`, getStatusOfRow(myRow))
				.done(function (data) {
					$('#stdout').text(data)
				})
		})

		//	Table Row Selection
		$('tr:not(.has-text-centered)').click(function () {
			let selectedIndoor = $(this).find('span.address').text()

			//	In case of selected objects, deselect it.
			if ($(this).hasClass('is-selected')) {
				$(this).removeClass('is-selected')
				$(this).find('.button').removeClass('is-inverted')
				selectedIndoors.delete(selectedIndoor)
			}
			else {
				$(this).addClass('is-selected')
				$(this).find('.button').addClass('is-inverted')
				selectedIndoors.add(selectedIndoor)
			}

			//	Emit selectedIndoorsChanged event
			$('#groupController').trigger('selectedIndoorsChanged')
		})

		//	Power Button
		$('.power').click(function () {
			event.stopPropagation()
			$(this).toggleClass('is-info is-danger')

			// TODO: Send a Request
			const myRow = $(this).parents('tr')
			$.post(`/api/controllers/${controller}/indoors/${myRow.find('span.address').text()}`, getStatusOfRow(myRow))
				.done(function (data) {
					$('#stdout').text(data)
				})
		})

		//	airSwingUD Button
		$('.airSwingUD').click(function () {
			event.stopPropagation()
			$(this).toggleClass('is-warning is-active')

			// TODO: Send a Request
			const myRow = $(this).parents('tr')
			$.post(`/api/controllers/${controller}/indoors/${myRow.find('span.address').text()}`, getStatusOfRow(myRow))
				.done(function (data) {
					$('#stdout').text(data)
				})
		})

		//	airSwingLR Button
		$('.airSwingLR').click(function () {
			event.stopPropagation()
			$(this).toggleClass('is-warning is-active')

			// TODO: Send a Request
			const myRow = $(this).parents('tr')
			$.post(`/api/controllers/${controller}/indoors/${myRow.find('span.address').text()}`, getStatusOfRow(myRow))
				.done(function (data) {
					$('#stdout').text(data)
				})
		})
	})
}
